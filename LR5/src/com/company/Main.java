package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	HashMap<String, Integer> map = new HashMap<>();
	map.put("Three", 3);
	map.put("One", 1);
	map.put("Two", 2);
	map.put("Four", 2);
	printMap(map);
	System.out.println("Теряется Two или Four, так как у них одинаковое значение. Но я сделал так, как было показано " +
            "в примере в телеге. Могу переделать, если неправильно понял задание");
	HashMap<ArrayList<Integer>, String> res = method(map);
	printMap(res);
    }

    private static <K, V> HashMap<ArrayList<V>, K> method(HashMap<K, V> map) {
        HashMap<ArrayList<V>, K> result = new HashMap<>();
        for (Map.Entry<K, V> pair : map.entrySet()) {
            K key = pair.getKey();
            V value = pair.getValue();
            if (result.isEmpty()) {
                ArrayList<V> forKeys = new ArrayList<>();
                forKeys.add(value);
                result.put(forKeys, key);
            } else {
                boolean flag = false;
                for (ArrayList<V> resKey : result.keySet())
                    if (resKey.contains(value)) {
                        resKey.add(value);
                        flag = true;
                        break;
                    }
                if (!flag) {
                    ArrayList<V> forKeys = new ArrayList<>();
                    forKeys.add(value);
                    result.put(forKeys, key);
                }
            }
        }
        return result;
    }

    private static <K, V> void printMap(HashMap<K, V> map) {
        for (Map.Entry<K, V> pair : map.entrySet()) System.out.println(pair.getKey() + " : " + pair.getValue());
    }
}
